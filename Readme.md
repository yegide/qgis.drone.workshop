# Taking full advantage of drone imagery with QGIS

## Download workshop materials

All the workshop contents is stored in a git repository. Please install git to get the workshop data.

Windows users can install git from [git for Windows](https://git-scm.com/downloads). Windows users should use the power shell.

After installing git, please run:

```
git clone https://gitlab.com/jgrocha/qgis.drone.workshop.git
```

You will get a new folder called `qgis.drone.workshop`.

## Installing QGIS Plugins

1. ImportPhotos
2. MapSwipe
3. Profile tool
4. Point sampling tool
5. Raster value

## The initial data

Open QGIS.

Use the Import Photo plugin to load the image location. 

1. Style the images with the image name as label. 
1. Style the RelPath field as an Attachment

Use the Add delimited text to add the ground control points.

1. Style the GCP with its number as label.
1. STyle the GCP as circles with only strike color (transparent filling).

Use the points to path processing to create the drone path.

1. Use Points to path processing algoritm

## Install WebODM

To install WebODM, we will need:

1. Docker
2. Python

### Install Docker (for Windows users)

Windows users should install docker from [Docker for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows).

After installing Docker, click on Docker tray icon and select "Switch to Linux Containers".

Go to Settings, and select 4 cores and at least 4 Gb of RAM.

### Install Docker (for Ubuntu users)

```
sudo apt install docker-ce
sudo systemctl status docker
```

#### To execute Docker without sudo

```
sudo usermod -aG docker ${USER}
su - ${USER}
```

### Install Python (for Windows users)

Install [Python for Windows](https://www.python.org/downloads/windows/). Use the [Windows x86-64 executable installer](https://www.python.org/ftp/python/3.7.2/python-3.7.2-amd64.exe).

Make sure you select the option: "Add Python 3.x to your path".

## Run WebODM

[WebODM home page](https://github.com/OpenDroneMap/WebODM)

```
git clone https://github.com/OpenDroneMap/WebODM --config core.autocrlf=input --depth 1
cd WebODM
./webodm.sh update
```

It will take some time to download the WebODM image. Wait until the image if fully downloaded.

```
./webodm.sh start
```

After the service start, you can use WebODM from: http://localhost:8000

### New WebODM project

### New task

### Explore the results within WebODM

# Processing

## Processing with LAStools

## Processing with PDAL

## Geoprocessing

### Pretty hillshade map

### Calculate heights using raster values

### Calculate blue spots (using SAGA)

### Calculate catchment areas and streams

### Calculate volumes




